name := "my-scalatra"

version := "1.0"

scalaVersion := "2.11.8"


crossPaths := false
artifactName := {(version: ScalaVersion, module: ModuleID, artifact: Artifact) =>
  s"${artifact.name}.${artifact.extension}"
}

lazy val deploy = taskKey[Unit]("deploy")
deploy := {
  "docker cp target/my-scalatra.war demo-api:/usr/local/tomcat/webapps/ROOT.war" !
}

val scalatraV = "2.4.0"

libraryDependencies ++= Seq(
  "javax.servlet"                  % "javax.servlet-api"      % "3.1.0" % "provided",
  "org.scalatra"                   %% "scalatra"              % scalatraV,
  "org.scalatra"                   %% "scalatra-json"         % scalatraV,
  "org.json4s"                     %% "json4s-jackson"        % "3.3.0",
  "ch.qos.logback"                 %  "logback-classic"       % "1.1.7"               % "runtime",
  "com.typesafe.scala-logging"     %% "scala-logging"         % "3.4.0",
  "mysql"                          % "mysql-connector-java"   % "5.1.39",
  "com.github.aselab"              %% "scala-activerecord"    % "0.3.1"

)

containerLibs in Tomcat := Seq(
  "com.github.jsimone"            % "webapp-runner"             % "8.0.33.0" intransitive()
)

enablePlugins(TomcatPlugin)

package demo.models

import com.github.aselab.activerecord._


case class Product(name: String, brand: String) extends ActiveRecord {
  lazy val variants = hasMany[Variant]
}


object Product extends ActiveRecordCompanion[Product]
package demo.models

import com.github.aselab.activerecord._


case class Variant(size: String, color: String) extends ActiveRecord {
  val productId: Long = 0
  lazy val product = belongsTo[Product]
  lazy val collections = hasAndBelongsToMany[Collection]
}
object Variant extends ActiveRecordCompanion[Variant]
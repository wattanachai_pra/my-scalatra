package demo.models

import com.github.aselab.activerecord._
import com.github.aselab.activerecord.dsl._

object Tables extends ActiveRecordTables {
  val products = table[Product]
  val variants = table[Variant]
  val collections = table[Collection]

}

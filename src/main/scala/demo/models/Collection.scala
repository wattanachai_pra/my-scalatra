package demo.models

import com.github.aselab.activerecord.{ActiveRecord, ActiveRecordCompanion}

/**
  * Created by BiG on 6/17/2016 AD.
  */
case class Collection (name: String) extends ActiveRecord{
  val parentId: Option[Long] = None
  lazy val variants = hasAndBelongsToMany[Variant]
  lazy val children = hasMany[Collection](foreignKey = "parentId")
  lazy val parent = belongsTo[Collection](foreignKey = "parentId")
}
object Collection extends ActiveRecordCompanion[Collection]
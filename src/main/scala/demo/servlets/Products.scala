package demo.servlets

import com.github.aselab.activerecord.dsl._
import com.typesafe.scalalogging.LazyLogging
import demo.models._
import org.json4s._
import org.scalatra.json.JacksonJsonSupport
import org.scalatra.{CorsSupport, ScalatraServlet}

/**
  * Created by BiG on 6/16/2016 AD.
  */
class Products extends ScalatraServlet with JacksonJsonSupport with CorsSupport with LazyLogging {
  protected implicit lazy val jsonFormats: Formats = DefaultFormats

  protected override def transformResponseBody(body: JValue): JValue = body.underscoreKeys

  before() {
    contentType = formats("json")
  }


  get("/") {
    val products = Product.all

    logger.info("Get all products")

    Map("message" -> "success", "data" -> products.asJson)
  }


  get("/:id") {
    val id = params("id").toLong
    val product = Product.find(id).get
    Map(
      "message" -> "success",
      "data" -> (product.toMap + ("variants" -> product.variants.asJson))
    )
  }

  post("/") {
    val body = parsedBody.extract[Map[String, Any]]
    val name = body("name").toString
    val brand = body("brand").toString

    val product = Product(name, brand)
    product.save()

    Map("message" -> "success", "data" -> product.asJson)
  }

  get("/redirect") {
    halt(400, "Not allow to redirect")
    redirect("http://google.com")
  }

  options("/*") {
    response.setHeader(
      "Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers")
    )
  }
}

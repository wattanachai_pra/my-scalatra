package demo.servlets

import demo.models.{Collection, Variant}
import org.json4s.{DefaultFormats, Formats, JValue}
import org.scalatra.{CorsSupport, ScalatraServlet}
import org.scalatra.json.JacksonJsonSupport
import com.github.aselab.activerecord.dsl._
/**
  * Created by BiG on 6/17/2016 AD.
  */

class Collections extends ScalatraServlet with JacksonJsonSupport with CorsSupport {
  protected implicit lazy val jsonFormats: Formats = DefaultFormats
  protected override def transformResponseBody(body: JValue): JValue = body.underscoreKeys

  before() {
    contentType = formats("json")
  }
//Get all Collection
  get("/") {
    val collections = Collection.all
    Map("message" -> "success", "data" -> collections.asJson)
  }

//Create Collections
  post("/") {
    val body = parsedBody.extract[Map[String, Any]]
    val collectionName = body("name").toString
    val collection = Collection(collectionName)
    collection.save()
    Map("message" -> "success", "data" -> collection.asJson)
  }

  //Get Collection By Id
  get("/:id") {
    val id = params("id").toInt
    val collection = Collection.find(id).get

    val parent = collection.parentId.isDefined match {
      case true => collection.parent.asJson
      case _ => None
    }

    Map(
      "message" -> "success",
      "data" -> (
        collection.toMap
          + ("variants" -> collection.variants.asJson)
          + ("parent" -> parent)
          + ("children" -> collection.children.asJson)
        )
    )
  }

  //Create children collection
  post("/:id/children") {
    val id = params("id").toLong
    val body = parsedBody.extract[Map[String, Any]]

    val collection = Collection.find(id).get
    collection.children << Collection(body("name").toString)
    collection.save

    Map(
      "message" -> "success",
      "data" -> (collection.toMap + ("children" -> collection.children.asJson))
    )
  }

  //Delete all variant from collection
  delete("/:cid/variants") {
    val cid = params("cid").toLong

    val collection = Collection.find(cid).get
    collection.variants.removeAll()
    collection.save

    Map(
      "message" -> "success",
      "data" -> (collection.toMap + ("variants" -> collection.variants.asJson))
    )
  }
//delete specific variant from collection
  delete("/:cid/variants/:vid") {
    val vid = params("vid").toLong
    val cid = params("cid").toLong

    val variant = Variant.find(vid).get
    val collection = Collection.find(cid).get

    collection.variants.remove(variant)
    collection.save

    Map(
      "message" -> "success",
      "data" -> (collection.toMap + ("variants" -> collection.variants.asJson))
    )
  }

  //move collection to another parent
  put("/:cid/parent/:pid") {
    val pid = params("pid").toLong
    val cid = params("cid").toLong

    val parent = Collection.find(pid).get
    val collection = Collection.find(cid).get

    collection.parent := parent
    collection.save

    Map(
      "message" -> "success",
      "data" -> (collection.toMap + ("parent" -> collection.parent.asJson))
    )
  }

  //Add variant to collection
  put("/:cid/variants/:vid") {
    val vid = params("vid").toLong
    val cid = params("cid").toLong

    val variant = Variant.find(vid).get
    val collection = Collection.find(cid).get

    collection.variants.find(vid).isEmpty match {
      case true =>
        collection.variants << variant
        collection.save
      case _ =>
    }

    Map(
      "message" -> "success",
      "data" -> (collection.toMap + ("variants" -> collection.variants.asJson))
    )
  }

}
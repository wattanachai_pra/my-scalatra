package demo.servlets

import com.github.aselab.activerecord.dsl._
import demo.models._
import org.json4s._
import org.scalatra.json.JacksonJsonSupport
import org.scalatra.{CorsSupport, ScalatraServlet}

class Variants extends ScalatraServlet with JacksonJsonSupport with CorsSupport {
  protected implicit lazy val jsonFormats: Formats = DefaultFormats

  protected override def transformResponseBody(body: JValue): JValue = body.underscoreKeys

  before() {
    contentType = formats("json")
  }

  get("/:id") {
    val id = params("id").toLong
    val variant = Variant.find(id).get
    Map(
      "message" -> "success",
      "data" -> (variant.toMap
        + ("product" -> variant.product.toMap)
        + ("collections" -> variant.collections.asJson)
        )
    )
  }
}

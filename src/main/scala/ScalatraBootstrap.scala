import javax.servlet.ServletContext

import demo.models.Tables
import demo.servlets.{Collections, Products, Variants}
import org.scalatra.LifeCycle


class ScalatraBootstrap extends LifeCycle {

  System.setProperty("run.mode", "docker")

  override def init(context: ServletContext): Unit = {
    Tables.initialize
    context.mount(new Products, "/products/*")
    context.mount(new Variants, "/variants/*")
    context.mount(new Collections, "/collections/*")
  }

  override def destroy(context: ServletContext): Unit = {
    Tables.cleanup
  }
}
